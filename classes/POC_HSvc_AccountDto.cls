/** Class Name   : POC_HSvc_AccountDto *****
 *  Description  : Convenience class used to move data between classes and application architecture layers. Mimics the structure of the custom settings
 *  Created By   : Neeharik Polisetti
 *  Created On   : 09-30-2014
 
 
 /** Class Name   : POC_HSvc_AccountDto ***
 *  Description  : POC Jenkins
 *  Created By   : Anima
 *  Created On   : 29-07-2016

 *  Modification Log:  
 *  --------------------------------------------------------------------------------------------------------------------------------------
 *   Developer                Date                   Modification ID      Description 
 *  ---------------------------------------------------------------------------------------------------------------------------------------
 *   Avinash Yaramilli		12-Feb-15				M-0001				Added get; set to Account instance                                                                               
 **/

public class POC_HSvc_AccountDto {
	//M-0001
	public Account acc{get;set;}
	//End of M-0001
}